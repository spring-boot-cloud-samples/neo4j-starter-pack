package in.silentsudo.neo4jstarterpack;

import in.silentsudo.neo4jstarterpack.persistence.graph.Person;
import in.silentsudo.neo4jstarterpack.persistence.graph.PersonRepository;
import in.silentsudo.neo4jstarterpack.persistence.graph.cinema.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@Slf4j
@SpringBootApplication
public class Neo4jStarterPackApplication implements CommandLineRunner {

    private final PersonRepository personRepository;

    private final MovieRepository movieRepository;

    private final ActorRepository actorRepository;

    @Autowired
    public Neo4jStarterPackApplication(PersonRepository personRepository, MovieRepository movieRepository, ActorRepository actorRepository) {
        this.personRepository = personRepository;
        this.movieRepository = movieRepository;
        this.actorRepository = actorRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(Neo4jStarterPackApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        actorRepository.deleteAll();
        movieRepository.deleteAll();
        personRepository.deleteAll();

        Person ronny = new Person("Ronny");
        Person mony = new Person("Mony");
        Person shonny = new Person("Shonny");

        List<Person> people = Arrays.asList(ronny, mony, shonny);

        people.forEach(personRepository::save);
        log.info("Before linking");
        print(people);
        ronny.addTeamMate(mony);
        ronny.addTeamMate(shonny);

        mony.addTeamMate(shonny);

        personRepository.save(ronny);
        personRepository.save(mony);

        log.info("Before linking");
        print(people);

        Movie titanic = new Movie("Titanic", "The Movie", Collections.emptySet());

        Actor jack = new Actor("Leonardo DiCaprio", null);
        Actor kate = new Actor("Kate Winslet", null);

        Role leadActorRole = new Role("Lead Actor", jack, titanic);
        Role leadActressRole = new Role("Lead Actress", kate, titanic);


        titanic.setRoles(new HashSet<>(Arrays.asList(leadActorRole, leadActressRole)));

        movieRepository.save(titanic);


    }

    public void print(List<Person> people) {
//        people.forEach(p -> {
//            log.info("*************************");
//            log.info("Person: {}", p.getName());
//            log.info("Teammates: ");
//            Optional.ofNullable(p.getTeammates())
//                    .orElse(Collections.emptySet())
//                    .forEach(tm -> {
//                        log.info("\t\t\t{}", tm.getName());
//                    });
//            log.info("*************************");
//        });
    }
}
