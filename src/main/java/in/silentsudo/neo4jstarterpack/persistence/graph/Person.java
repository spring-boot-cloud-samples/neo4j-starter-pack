package in.silentsudo.neo4jstarterpack.persistence.graph;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.HashSet;
import java.util.Set;

@NodeEntity
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    @Id
    @GeneratedValue
    @Getter
    private Long id;

    @Setter
    @Getter
    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Getter
    @JsonIgnoreProperties("teammates")
    @Relationship(value = "TEAMMATES", direction = Relationship.UNDIRECTED)
    private Set<Person> teammates;

    public void addTeamMate(@NonNull Person person) {
        if (teammates == null) {
            teammates = new HashSet<>();
        }
        teammates.add(person);
    }
}
