package in.silentsudo.neo4jstarterpack.persistence.graph.cinema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import in.silentsudo.neo4jstarterpack.persistence.graph.Neo4jEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
@RelationshipEntity("ACTED_IN")
public class Role extends Neo4jEntity {
    private String name;

    @StartNode
    private Actor actor;

    @JsonIgnore
    @EndNode
    private Movie movie;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (!name.equals(role.name)) return false;
        if (!actor.equals(role.actor)) return false;
        return movie.equals(role.movie);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + actor.hashCode();
        result = 31 * result + movie.hashCode();
        return result;
    }
}
