package in.silentsudo.neo4jstarterpack.persistence.graph.cinema;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.neo4j.annotation.QueryResult;

@QueryResult
@AllArgsConstructor
@NoArgsConstructor
public class ActorInfo {

    @Getter
    @Setter
    private String role;

    @Getter
    @Setter
    private String actorName;
}
