package in.silentsudo.neo4jstarterpack.persistence.graph.cinema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import in.silentsudo.neo4jstarterpack.persistence.graph.Neo4jEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NodeEntity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Actor extends Neo4jEntity {
    private String name;

    @JsonIgnoreProperties("movies")
    @Relationship("ACTED_IN")
    private List<Movie> movies;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Actor actor = (Actor) o;

        return name.equals(actor.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}

