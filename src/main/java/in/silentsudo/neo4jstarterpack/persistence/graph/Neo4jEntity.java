package in.silentsudo.neo4jstarterpack.persistence.graph;

import lombok.Getter;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;

public abstract class Neo4jEntity {
    @Id
    @Getter
    @GeneratedValue
    private Long id;

}