package in.silentsudo.neo4jstarterpack.persistence.graph.cinema;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import in.silentsudo.neo4jstarterpack.persistence.graph.Neo4jEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@NodeEntity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Movie extends Neo4jEntity {
    private String name;
    private String tagline;

    @JsonIgnoreProperties("roles")
    @Relationship(value = "ACTED_IN", direction = Relationship.INCOMING)
    private Set<Role> roles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (!name.equals(movie.name)) return false;
        return tagline.equals(movie.tagline);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + tagline.hashCode();
        return result;
    }
}