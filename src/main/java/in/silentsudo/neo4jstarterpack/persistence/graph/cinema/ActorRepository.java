package in.silentsudo.neo4jstarterpack.persistence.graph.cinema;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActorRepository extends Neo4jRepository<Actor, Long> {
    Optional<Actor> findByName(String name);

    /**
     * Find actor[role, name] by movie name
     * MATCH (a:Actor)-[r:ACTED_IN]->(m:Movie) where m.name = 'Titanic'
     * RETURN r.name as role, a.name as actor_name
     */

    @Query("MATCH (a:Actor)-[r:ACTED_IN]->(m:Movie) WHERE m.name = 'Titanic' " +
            "RETURN r.name AS role, a.name AS actorName")
    Optional<List<ActorInfo>> findActorInfoByMovieName();
}
