package in.silentsudo.neo4jstarterpack.controllers;

import in.silentsudo.neo4jstarterpack.persistence.graph.Person;
import in.silentsudo.neo4jstarterpack.persistence.graph.PersonRepository;
import in.silentsudo.neo4jstarterpack.persistence.graph.cinema.ActorInfo;
import in.silentsudo.neo4jstarterpack.persistence.graph.cinema.ActorRepository;
import in.silentsudo.neo4jstarterpack.persistence.graph.cinema.Movie;
import in.silentsudo.neo4jstarterpack.persistence.graph.cinema.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class HelloController {
    private final PersonRepository personRepository;
    private final MovieRepository movieRepository;
    private final ActorRepository actorRepository;

    @GetMapping("/world")
    public String helloWorld() {
        return "Hello World!";
    }

    @GetMapping("/people")
    public Iterable<Person> get() {
        return personRepository.findAll();
    }

    @GetMapping("/people/count")
    public long count() {
        return personRepository.count();
    }

    @GetMapping("/movies")
    public Iterable<Movie> getMovies() {
        return movieRepository.findAll();
    }

    @GetMapping("/actor-info")
    public Iterable<ActorInfo> getActorInfo() {
        return actorRepository
                .findActorInfoByMovieName()
                .orElse(Collections.emptyList());
    }
}
